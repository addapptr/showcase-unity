﻿using UnityEngine;
using UnityEngine.UI;

public class AATKitDemo : MonoBehaviour
{
	public enum DemoState
	{
		Init = 0,
		Banner = 1,
		Fullscreen = 2,
		Multisize = 3
	}

	public GameObject InitPanel;
	public GameObject BannerPannel;
	public GameObject FulscreenPanel;
	public GameObject MultiSizePanel;
	public Text versionText;
	public Text shakeText;
	public Text autoreloadBannerText;
	public Text autoreloadInterstitialText;
	public Text autoreloadMultiSizeText;

	DemoState currentState = DemoState.Init;
	const string bannerPlacement = "BannerPlacement";
	const string interstitialPlacement = "FullscreenPlacement";
	const string multiSizePlacement = "MultiSizePlacement";
	bool initialized = false;
	bool shakeEnabled = false;
	AATKitBinding.BannerAlignment bannerAlignment = AATKitBinding.BannerAlignment.BottomCenter;
	AATKitBinding.BannerAlignment multiSizeBannerAlignment = AATKitBinding.BannerAlignment.BottomCenter;
	bool bannerAutoreload = false;
	bool multiSizeAutoreload = false;
	bool interstitialAutoreload = false;

	void Start()
	{
		versionText.text = "Version " + AATKitBinding.GetVersion();
	}

	public void OnBackClicked()
    {
		InitPanel.SetActive(true);

		switch(currentState)
        {
			case DemoState.Banner:
				BannerPannel.SetActive(false);
				break;
			case DemoState.Fullscreen:
				FulscreenPanel.SetActive(false);
				break;
			case DemoState.Multisize:
				MultiSizePanel.SetActive(false);
				break;
		}

		currentState = DemoState.Init;
    }

	public void OnBannerClicked()
	{
		InitPanel.SetActive(false);
		BannerPannel.SetActive(true);
		currentState = DemoState.Banner;
	}

	public void OnFullscreenClicked()
	{
		InitPanel.SetActive(false);
		FulscreenPanel.SetActive(true);
		currentState = DemoState.Fullscreen;
	}

	public void OnMultisizeClicked()
	{
		InitPanel.SetActive(false);
		MultiSizePanel.SetActive(true);
		currentState = DemoState.Multisize;
	}

	public void InitAATKit()
    {
        if (!initialized)
        {
            AATKitConfiguration aatkitConfiguration = new AATKitConfiguration
            {
                TestModeAccountId = 74,
                Consent = new AATKitConsent()
                {
                    Type = AATKitConsent.ConsentTypes.ManagedCMPGoogle
                }
            };

            RegisterAATKitDelegates();
            AATKitBinding.Init(aatkitConfiguration);
            AATKitBinding.SetDebugEnabled();
            AATKitBinding.EditConsent();
            Vector2 size = AATKitBinding.CreatePlacement(bannerPlacement, AATKitBinding.PlacementSize.BannerAuto);
            Vector2 size2 = AATKitBinding.CreatePlacement(interstitialPlacement, AATKitBinding.PlacementSize.Fullscreen);
            AATKitBinding.CreatePlacement(multiSizePlacement, AATKitBinding.PlacementSize.MultiSizeBanner);

            Debug.Log("Banner size: " + size.x + " x " + size.y);
            Debug.Log("Fullscreen size: " + size2.x + " x " + size2.y);

            initialized = true;
        }
    }

	private void RegisterAATKitDelegates()
	{
		AATKitBinding.OnHaveAdDelegate += OnHaveAd;
		AATKitBinding.OnHaveAdOnMultiSizeBannerDelegate += OnHaveAdOnMultiSizeBanner;
		AATKitBinding.OnNoAdDelegate += OnNoAd;
		AATKitBinding.OnPauseForAdDelegate += OnPauseForAd;
		AATKitBinding.OnResumeAfterAdDelegate += OnResumeAfterAd;
		AATKitBinding.OnShowingEmptyDelegate += OnShowingEmpty;
		AATKitBinding.OnUserEarnedIncentiveDelegate += OnUserEarnedIncentive;
		AATKitBinding.OnObtainedAdRulesDelegate += OnObtainedAdRules;
		AATKitBinding.OnManagedConsentNeedsUserInterfaceDelegate += OnManagedConsentNeedsUserInterface;
		AATKitBinding.OnManagedConsentCMPFinishedDelegate += OnManagedConsentCMPFinished;
		AATKitBinding.OnManagedConsentCMPFailedToLoadDelegate += OnManagedConsentCMPFailedToLoad;
		AATKitBinding.OnManagedConsentCMPFailedToShowDelegate += OnManagedConsentCMPFailedToShow;
	}

	public void EnableDebugLog()
    {
        if (initialized)
        {
            AATKitBinding.SetDebugEnabled();
        }
    }

	public void EnableDebugShake()
    {
        if (initialized)
        {
            if (shakeEnabled)
            {
                AATKitBinding.SetDebugShakeEnabled(false);
                shakeEnabled = false;
                shakeText.text = "Enable Debug Shake";
            }
            else
            {
                AATKitBinding.SetDebugShakeEnabled(true);
                shakeEnabled = true;
                shakeText.text = "Disable Debug Shake";
            }
        }
    }

	public void ReloadBanner()
    {
        if (initialized)
        {
            AATKitBinding.ReloadPlacement(bannerPlacement);
        }
    }

	public void EnableBannerAutoReload()
    {
        if (initialized)
        {
            if (bannerAutoreload)
            {
                AATKitBinding.StopPlacementAutoReload(bannerPlacement);
                bannerAutoreload = false;
                autoreloadBannerText.text = "Enable Auto-Reload";
            }
            else
            {
                AATKitBinding.StartPlacementAutoReload(bannerPlacement);
                bannerAutoreload = true;
                autoreloadBannerText.text = "Disable Auto-Reload";
            }
        }
    }

	public void ChangeBannerAlignment()
    {
        if (initialized)
        {
            bannerAlignment = GetNewAlignment(bannerAlignment);
            AATKitBinding.SetPlacementAlignment(bannerPlacement, bannerAlignment);
        }
    }

	public void ReloadFullscreen()
    {
        if (initialized)
        {
            AATKitBinding.ReloadPlacement(interstitialPlacement);
        }
    }

	public void EnableFullscreenAutoReload()
    {
        if (interstitialAutoreload)
        {
            AATKitBinding.StopPlacementAutoReload(interstitialPlacement);
            interstitialAutoreload = false;
            autoreloadInterstitialText.text = "Enable Auto-Reload";
		}
        else
        {
            AATKitBinding.StartPlacementAutoReload(interstitialPlacement);
            interstitialAutoreload = true;
            autoreloadInterstitialText.text = "Disable Auto-Reload";
		}
    }

	public void ShowFullscreen()
    {
        if (initialized)
        {
            AATKitBinding.ShowPlacement(interstitialPlacement);
        }
    }

	public void ReloadMultisize()
    {
        if (initialized)
        {
            AATKitBinding.ReloadPlacement(multiSizePlacement);
        }
    }

	public void EnableMultisizeAutoReload()
    {
        if (multiSizeAutoreload)
        {
            AATKitBinding.StopPlacementAutoReload(multiSizePlacement);
            multiSizeAutoreload = false;
            autoreloadMultiSizeText.text = "Enable Auto-Reload";
		}
        else
        {
            AATKitBinding.StartPlacementAutoReload(multiSizePlacement);
            multiSizeAutoreload = true;
            autoreloadMultiSizeText.text = "Disable Auto-Reload";
		}
    }

	public void ChangeMultisizeAlignment()
    {
        if (initialized)
        {
            multiSizeBannerAlignment = GetNewAlignment(multiSizeBannerAlignment);
            AATKitBinding.SetMultiSizeAlignment(multiSizePlacement, multiSizeBannerAlignment);
        }
    }

    AATKitBinding.BannerAlignment GetNewAlignment (AATKitBinding.BannerAlignment alignment)
	{
		AATKitBinding.BannerAlignment newBannerAlignment = alignment;
		switch (alignment) {
		case AATKitBinding.BannerAlignment.BottomCenter:
			newBannerAlignment = AATKitBinding.BannerAlignment.BottomRight;
			break;
		case AATKitBinding.BannerAlignment.BottomRight:
			newBannerAlignment = AATKitBinding.BannerAlignment.BottomLeft;
			break;
		case AATKitBinding.BannerAlignment.BottomLeft:
			newBannerAlignment = AATKitBinding.BannerAlignment.TopCenter;
			break;
		case AATKitBinding.BannerAlignment.TopCenter:
			newBannerAlignment = AATKitBinding.BannerAlignment.TopRight;
			break;
		case AATKitBinding.BannerAlignment.TopRight:
			newBannerAlignment = AATKitBinding.BannerAlignment.TopLeft;
			break;
		case AATKitBinding.BannerAlignment.TopLeft:
			newBannerAlignment = AATKitBinding.BannerAlignment.BottomCenter;
			break;
		}
		return newBannerAlignment;
	}		

	/*
	 * aatkit events
	 * */
	public void OnHaveAd(string placementName) 
	{
		Debug.Log("onHaveAd event: " + placementName);
	}

	public void OnHaveAdOnMultiSizeBanner(string placementName) 
	{
		Debug.Log("onHaveAdOnMultiSizeBanner event: " + placementName);
	}
	
	public void OnNoAd(string placementName) 
	{
		Debug.Log("onNoAd event: " + placementName);
	}
	
	public void OnPauseForAd(string placementName) 
	{
		Debug.Log("onPauseForAd event: " + placementName);
	}
	
	public void OnResumeAfterAd(string placementName) 
	{
		Debug.Log("onResumeAfterAd event: " + placementName);
	}
	
	public void OnShowingEmpty(string placementName) 
	{
		Debug.Log("onShowingEmpty event: " + placementName);
	}

	public void OnUserEarnedIncentive(AATKitIncentiveData incentiveData) {
		Debug.Log("onUserEarnedIncentive event: " + incentiveData.ToString());
	}

	public void OnObtainedAdRules(bool fromTheServer) 
	{
		Debug.Log("onObtainedAdRules event: " + fromTheServer);
	}

    public void OnManagedConsentNeedsUserInterface()
    {
        Debug.Log("onManagedConsentNeedsUserInterface");
		AATKitBinding.ShowConsentDialogIfNeeded();
	}

	public void OnManagedConsentCMPFinished(AATKitBinding.ManagedConsentState state)
	{
		Debug.Log("onManagedConsentCMPFinished state: " + state);
	}

	public void OnManagedConsentCMPFailedToLoad(string error)
	{
		Debug.Log("onManagedConsentCMPFailedToLoad error: " + error);
	}

	public void OnManagedConsentCMPFailedToShow(string error)
	{
		Debug.Log("onManagedConsentCMPFailedToShow error: " + error);
	}
}
