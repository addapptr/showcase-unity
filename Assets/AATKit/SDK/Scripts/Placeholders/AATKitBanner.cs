﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AATKitBanner : MonoBehaviour, IAATKitPlacement
{
    private static readonly int IPadScreenWidth = 1536;

    private static readonly Dictionary<int, float> AndroidScaleByScreenWidth = new Dictionary<int, float>();

    private static readonly Dictionary<int, float> IphoneScaleByScreenWidth = new Dictionary<int, float>();

    private static readonly Dictionary<AATKitBinding.PlacementSize, Vector2> BannerSizeByPlacementSize = new Dictionary<AATKitBinding.PlacementSize, Vector2>();

    public AATKitBinding.PlacementSize PlacementSize
    {
        get
        {
            return placementSize;
        }
    }

    [SerializeField]
    private AATKitBinding.PlacementSize placementSize;

    [SerializeField]
    private RectTransform content;

    private RectTransform rect;

    private string placementName;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
        InitializeBannerSizeByPlacementSize();
        InitializeAndroidScaleByScreenWidth();
        InitializeIphoneScaleByScreenWidth();
    }

    private static void InitializeBannerSizeByPlacementSize()
    {
        if(BannerSizeByPlacementSize.Count == 0)
        {
            BannerSizeByPlacementSize.Add(AATKitBinding.PlacementSize.Banner320x53, new Vector2(320, 53));
            BannerSizeByPlacementSize.Add(AATKitBinding.PlacementSize.Banner300x250, new Vector2(300, 250));
            BannerSizeByPlacementSize.Add(AATKitBinding.PlacementSize.Banner768x90, new Vector2(768, 90));
            BannerSizeByPlacementSize.Add(AATKitBinding.PlacementSize.BannerAuto, new Vector2(0, 0));
            BannerSizeByPlacementSize.Add(AATKitBinding.PlacementSize.Fullscreen, new Vector2(0, 0));
            BannerSizeByPlacementSize.Add(AATKitBinding.PlacementSize.MultiSizeBanner, new Vector2(0, 0));
            BannerSizeByPlacementSize.Add(AATKitBinding.PlacementSize.Rewarded, new Vector2(0, 0));
        }
    }

    private static void InitializeAndroidScaleByScreenWidth()
    {
        if(AndroidScaleByScreenWidth.Count == 0)
        {
            AndroidScaleByScreenWidth.Add(1440, 4f); //xxxhdpi
            AndroidScaleByScreenWidth.Add(960, 3f); //xxhdpi
            AndroidScaleByScreenWidth.Add(720, 2f); //xhdpi
            AndroidScaleByScreenWidth.Add(480, 1.5f); //hdpi
            AndroidScaleByScreenWidth.Add(320, 1f); //mdpi
            AndroidScaleByScreenWidth.Add(200, 0.75f); //ldpi
        }
    }

    private static void InitializeIphoneScaleByScreenWidth()
    {
        if(IphoneScaleByScreenWidth.Count == 0)
        {
            IphoneScaleByScreenWidth.Add(1536, 2f); //iPad
            IphoneScaleByScreenWidth.Add(1125, 3f); //iPhone X
            IphoneScaleByScreenWidth.Add(1080, 2.608f); //iPhone 8 Plus, 7 Plus, 6s Plus
            IphoneScaleByScreenWidth.Add(640, 2f); //iPhone 7, 6s, 6, SE, 5s, 5, 4s, 4
        }
    }

    void Update()
    {
        if (Application.isEditor)
        {
#if UNITY_ANDROID
           
           UpdateBannerSize(AndroidScaleByScreenWidth);

#elif UNITY_IOS

            UpdateBannerSize(IphoneScaleByScreenWidth);

#endif
        }
    }

    private void UpdateBannerSize(Dictionary<int, float> scaleByScreenWidth)
    {
        int screenDeviceWidth = Math.Min(Screen.width, Screen.height);

        foreach (KeyValuePair<int, float> entry in scaleByScreenWidth)
        {
            if (screenDeviceWidth >= entry.Key)
            {
                Vector2 bannerSize = CalculateBannerSize();
                float scale = entry.Value;
                rect.sizeDelta = bannerSize * scale;
                break;
            }
        }
    }

    private Vector2 CalculateBannerSize()
    {
        Vector2 size = BannerSizeByPlacementSize[placementSize];

        if (placementSize == AATKitBinding.PlacementSize.BannerAuto)
        {
#if UNITY_ANDROID

            size =  BannerSizeByPlacementSize[AATKitBinding.PlacementSize.Banner320x53];

#elif UNITY_IOS
            int screenDeviceWidth = Math.Min(Screen.width, Screen.height);

            if (screenDeviceWidth >= IPadScreenWidth)
            {
                size = BannerSizeByPlacementSize[AATKitBinding.PlacementSize.Banner768x90];
            }
            else
            {
                size = BannerSizeByPlacementSize[AATKitBinding.PlacementSize.Banner320x53];
            }

#endif
        }

        return size;
    }

    public void Initialize(string placementName, AATKitBinding.PlacementSize placementSize)
    {
        gameObject.name = placementName;
        this.placementSize = placementSize;
        this.placementName = placementName;
    }

    public void Reload()
    {
        Show();
        AATKitPlaceholderManager.SendEventToDelegate(AATKitPlaceholderManager.OnHaveAdEventName, placementName);
    }

    public void StartAutoReload()
    {
        Show();
        AATKitPlaceholderManager.SendEventToDelegate(AATKitPlaceholderManager.OnHaveAdEventName, placementName);
    }

    public void AddPlacementToView()
    {
        Show();
    }

    private void Show()
    {
        content.gameObject.SetActive(true);
    }

    public void RemovePlacementFromView()
    {
        Hide();
    }

    private void Hide()
    {
        content.gameObject.SetActive(false);
    }

    public void SetPlacementAlignment(AATKitBinding.BannerAlignment bannerAlignment)
    {
        switch (bannerAlignment)
        {
            case AATKitBinding.BannerAlignment.TopLeft:
                SetPositionOnTheScreen(new Vector2(0, 1), new Vector2(0, 1), new Vector2(0, 1));
                break;
            case AATKitBinding.BannerAlignment.TopCenter:
                SetPositionOnTheScreen(new Vector2(0.5f, 1), new Vector2(0.5f, 1), new Vector2(0.5f, 1));
                break;
            case AATKitBinding.BannerAlignment.TopRight:
                SetPositionOnTheScreen(new Vector2(1, 1), new Vector2(1, 1), new Vector2(1, 1));
                break;
            case AATKitBinding.BannerAlignment.BottomLeft:
                SetPositionOnTheScreen(new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0));
                break;
            case AATKitBinding.BannerAlignment.BottomCenter:
                SetPositionOnTheScreen(new Vector2(0.5f, 0), new Vector2(0.5f, 0), new Vector2(0.5f, 0));
                break;
            case AATKitBinding.BannerAlignment.BottomRight:
                SetPositionOnTheScreen(new Vector2(1, 0), new Vector2(1, 0), new Vector2(1, 0));
                break;
        }
    }

    private void SetPositionOnTheScreen(Vector2 pivot, Vector2 anchorMin, Vector2 anchorMax)
    {
        rect.pivot = pivot;
        rect.anchorMin = anchorMin;
        rect.anchorMax = anchorMax;
        rect.anchoredPosition = Vector2.zero;
    }

    public void StopAutoReload()
    {
    }

    public bool ShowPlacement()
    {
        return false;
    }
}
