﻿using UnityEngine;

public class AATKitInterstitial : MonoBehaviour, IAATKitPlacement
{
    [SerializeField]
    private GameObject content;

    private bool loaded = false;

    private bool autoReload = false;

    private string placementName;

    public AATKitBinding.PlacementSize PlacementSize
    {
        get
        {
            return AATKitBinding.PlacementSize.Fullscreen;
        }
    }

    public void AddPlacementToView()
    {
    }

    public void Initialize(string placementName, AATKitBinding.PlacementSize placementSize)
    {
        gameObject.name = placementName;
        this.placementName = placementName;
    }

    public void Reload()
    {
        loaded = true;
        AATKitPlaceholderManager.SendEventToDelegate(AATKitPlaceholderManager.OnHaveAdEventName, placementName);
    }

    public void RemovePlacementFromView()
    {
    }

    public void SetPlacementAlignment(AATKitBinding.BannerAlignment bannerAlignment)
    {
    }

    public bool ShowPlacement()
    {
        bool placementShown = false;

        if(loaded)
        {
            if(!autoReload)
            {
                loaded = false;
            }

            placementShown = true;
            content.SetActive(true);
            AATKitPlaceholderManager.SendEventToDelegate(AATKitPlaceholderManager.OnPauseForAdEventName, placementName);
        }

        return placementShown;
    }

    public void StartAutoReload()
    {
        autoReload = true;
        loaded = true;
        AATKitPlaceholderManager.SendEventToDelegate(AATKitPlaceholderManager.OnHaveAdEventName, placementName);
    }

    public void StopAutoReload()
    {
        autoReload = false;
    }

    public void Close()
    {
        content.SetActive(false);
        AATKitPlaceholderManager.SendEventToDelegate(AATKitPlaceholderManager.OnResumeAfterAdEventName, placementName);

        if(autoReload)
        {
            AATKitPlaceholderManager.SendEventToDelegate(AATKitPlaceholderManager.OnHaveAdEventName, placementName);
        }
    }
}
