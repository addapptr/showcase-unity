﻿public interface IAATKitPlacement
{
    AATKitBinding.PlacementSize PlacementSize
    {
        get;
    }

    void Initialize(string placementName, AATKitBinding.PlacementSize placementSize);

    void Reload();

    void StartAutoReload();

    void StopAutoReload();

    void AddPlacementToView();

    void RemovePlacementFromView();

    void SetPlacementAlignment(AATKitBinding.BannerAlignment bannerAlignment);

    bool ShowPlacement();
}
