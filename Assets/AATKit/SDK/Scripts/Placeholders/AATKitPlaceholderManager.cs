﻿using System.Collections.Generic;
using UnityEngine;

public class AATKitPlaceholderManager : MonoBehaviour
{
    public static readonly string OnHaveAdEventName = "OnHaveAd";

    public static readonly string OnPauseForAdEventName = "OnPauseForAd";

    public static readonly string OnResumeAfterAdEventName = "OnResumeAfterAd";

    public static AATKitPlaceholderManager Instance 
    { 
        get; 
        private set; 
    }

    [SerializeField]
    private AATKitBanner bannerPrefab;

    [SerializeField]
    private AATKitInterstitial interstitialPrefab;

    private Dictionary<string, IAATKitPlacement> placements;

    private bool initialized = false;

    private GameObject eventsDelegate;

    void Awake()
    {
        if(!Application.isEditor || Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public static void SendEventToDelegate(string eventName, string arg = null)
    {
        if (Instance != null && Instance.eventsDelegate != null)
        {
            if(arg == null)
            {
                Instance.eventsDelegate.SendMessage(eventName);
            } 
            else
            {
                Instance.eventsDelegate.SendMessage(eventName, arg);
            }
        }
    }

    public void Initialize(string objectName)
    {
        Log("Initialize");

        if(!initialized)
        {
            placements = new Dictionary<string, IAATKitPlacement>();
            initialized = true;
            eventsDelegate = GameObject.Find(objectName);
        }
        else
        {
            Log("Ignoring Initialize method call. AATKit already initialized.");
       }
    }

    public void CreatePlacement(string placementName, AATKitBinding.PlacementSize placementSize)
    {
        Log("CreatePlacement placementName: " + placementName + " placementSize: " + placementSize);

        if(IsInitialized())
        {
            if(placements.ContainsKey(placementName))
            {
                Log("Cannot create placement. " + placementName + " already exists.");
                return;
            }

            if ( placementSize == AATKitBinding.PlacementSize.MultiSizeBanner
                || placementSize == AATKitBinding.PlacementSize.Rewarded)
            {
                Log("Ignoring creating placement. Placement size " + placementSize + " is not supported in the Unity Editor.");
                return;
            }

            if(placementSize == AATKitBinding.PlacementSize.Fullscreen)
            {
                AATKitInterstitial newInterstitial = Instantiate(interstitialPrefab, transform);
                newInterstitial.Initialize(placementName, placementSize);
                placements.Add(placementName, newInterstitial);
            } 
            else
            {
                AATKitBanner newPlacement = Instantiate(bannerPrefab, transform);
                newPlacement.Initialize(placementName, placementSize);
                placements.Add(placementName, newPlacement);
            }

        }
    }

    public void ReloadPlacement(string placementName)
    {
        Log("ReloadPlacement placementName: " + placementName);

        if(IsInitialized())
        {
            if(PlacementExists(placementName))
            {
                IAATKitPlacement palcement = placements[placementName];
                palcement.Reload();
            }
        }
    }

    public void StartPlacementAutoReload(string placementName)
    {
        Log("StartPlacementAutoReload placementName: " + placementName);

        if (IsInitialized())
        {
            IAATKitPlacement palcement = placements[placementName];
            palcement.StartAutoReload();
        }
    }

    public void StopPlacementAutoReload(string placementName)
    {
        Log("StartPlacementAutoReload placementName: " + placementName);

        if (IsInitialized())
        {
            IAATKitPlacement palcement = placements[placementName];
            palcement.StopAutoReload();
        }
    }

    public void AddPlacementToView(string placementName)
    {
        Log("AddPlacementToView placementName: " + placementName);

        if (IsInitialized())
        {
            if (PlacementExists(placementName))
            {
                IAATKitPlacement placement = placements[placementName];
                placement.AddPlacementToView();
            }
        }
    }

    public void RemovePlacementFromView(string placementName)
    {
        Log("RemovePlacementFromView placementName: " + placementName);

        if (IsInitialized())
        {
            if (PlacementExists(placementName))
            {
                IAATKitPlacement placement = placements[placementName];
                placement.RemovePlacementFromView();
            }
        }
    }

    public void SetPlacementAlignment(string placementName, AATKitBinding.BannerAlignment bannerAlignment)
    {
        Log("SetPlacementAlignment placementName: " + placementName + " bannerAlignment: " + bannerAlignment);

        if (IsInitialized() && PlacementExists(placementName))
        {
            IAATKitPlacement placement = placements[placementName];
            placement.SetPlacementAlignment(bannerAlignment);
        }
    }

    public bool ShowPlacement(string placementName)
    {
        Log("ShowPlacement placementName: " + placementName);

        if (IsInitialized() && PlacementExists(placementName))
        {
            IAATKitPlacement placement = placements[placementName];
            return placement.ShowPlacement();
        }

        return false;
    }

    private bool PlacementExists(string placementName)
    {
        if(placements.ContainsKey(placementName))
        {
            return true;
        }
        else
        {
            Log(placementName + " does not exists");
            return false;
        }
    }

    private bool IsInitialized()
    {
        if(initialized)
        {
            return true;
        }
        else
        {
            Log("Error: AAtKit not initialized.");
            return false;
        }
    }

    private static void Log(string message)
    {
        Debug.Log("[" + typeof(AATKitPlaceholderManager) + "] " + message);
    }
}
