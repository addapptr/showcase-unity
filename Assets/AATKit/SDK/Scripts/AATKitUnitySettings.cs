﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class AATKitUnitySettings : ScriptableObject
{
    private static List<AATKitNetworkConfiguration> supportedNetworks = new List<AATKitNetworkConfiguration>
    {
        new AATKitNetworkConfiguration
            (
                networkName: "AdColony",
                androidFiles: new string[] { "AdColony.aar" },
                iOSPods: new string[] { "AATKit/AdColony" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "AdMob",
                iOSPods: new string[] { "AATKit/Admob" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "DFP",
                iOSPods: new string[] { "AATKit/DFP" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "AdX",
                iOSPods: new string[] { "AATKit/AdX" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "AppLovin",
                androidFiles: new string[] { "AppLovin.aar" },
                iOSPods: new string[] { "AATKit/AppLovinSDK" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "AppNexus",
                androidFiles: new string[] { "appnexus-sdk.aar" },
                iOSPods: new string[] { "AATKit/AppNexus" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "BlueStack",
                androidFiles: new string[] { "bluestack-core-sdk.aar" },
                iOSPods: new string[] { "AATKit/BlueStack" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "Criteo",
                androidFiles: new string[] { "CriteoSDK.aar" },
                iOSPods: new string[] { "AATKit/CriteoSDK" },
                iOSEnabled: false
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "Facebook",
                androidNetwork: false,
                iOSPods: new string[] { "AATKit/Facebook" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "InLoco",
                androidFiles: new string[] {
                    "inloco-android-sdk-ads-4.6.1.aar",
                    "inloco-android-sdk-core-4.6.1.aar",
                    "inloco-android-sdk-location-4.6.1.aar"
                },
                iOSNetwork: false
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "Inmobi",
                androidFiles: new string[] { "InMobi.aar" },
                iOSPods: new string[] { "AATKit/Inmobi" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "MoPub",
                androidFiles: new string[] {
                    "mopub-sdk-banner.aar",
                    "mopub-sdk-base.aar",
                    "mopub-sdk-native-static.aar",
                    "mopub-sdk-fullscreen.aar",
                    "mopub-omsdk-android.aar",
                    "mopub-volley.jar",
                    "libAvid-mopub.jar",
                    "mopub-sdk-networking.aar",
                    "mopub-sdk-util.aar"
                },
                iOSPods: new string[] { "AATKit/Mopub" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "Ogury",
                androidFiles: new string[] { "Ogury.aar" },
                iOSPods: new string[] { "AATKit/OguryAds" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "PubNative",
                iOSPods: new string[] { "AATKit/PubNative" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "Smaato",
                iOSPods: new string[] { "AATKit/Smaato" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "SmartAd",
                androidFiles: new string[] { "SmartAdServer.aar" },
                iOSPods: new string[] { "AATKit/SmartAdServer" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "Unity Ads",
                androidFiles: new string[] { "UnityAds.aar" },
                iOSPods: new string[] { "AATKit/Unity" }
            ),
        new AATKitNetworkConfiguration
            (
                networkName: "Yandex",
                androidFiles: new string[] {"yandex-mobileads.aar", "yandex-mobmetricalib.aar"},
                iOSPods: new string[] { "AATKit/Yandex" }
            )
    };

    public string version;

    public List<AATKitNetworkConfiguration> networks;

    public string gadApplicationIdentifier = string.Empty;

    public List<string> DirectoriesToIgnoreForIOS
    {
        get
        {
            List<string> directories = new List<string>();

            foreach (AATKitNetworkConfiguration network in networks)
            {
                if (network.iOSNetwork && !network.iOSEnabled)
                {
                    foreach (string directory in network.iOSDirectories)
                    {
                        directories.Add(directory);
                    }
                }
            }

            return directories;
        }
    }

    public List<string> PodsForEnabledNetworks
    {
       get
        {
            List<string> enabledPods = new List<string>();

            foreach (AATKitNetworkConfiguration network in networks)
            {
                if (network.iOSNetwork && network.iOSEnabled)
                {
                    foreach (string pod in network.iOSPods)
                    {
                        enabledPods.Add(pod);
                    }
                }
            }

            return enabledPods;
        }
    }

    public bool IsSwiftNeeded
    {
        get
        {
            bool result = false;

            foreach (AATKitNetworkConfiguration network in networks)
            {
                if (network.networkName.Equals("Criteo") && network.iOSEnabled)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }
    }

    public List<string> FilesToDisableForAndroid
    {
        get
        {
            return GetAndroidFiles(false);
        }
    }

    public List<string> FilesToEnableForAndroid
    {
        get
        {
            return GetAndroidFiles(true);
        }
    }

    private List<string> GetAndroidFiles(bool enabled)
    {
        List<string> files = new List<string>();

        foreach (AATKitNetworkConfiguration network in networks)
        {
            if (network.androidNetwork && network.androidEnabled == enabled)
            {
                foreach (string file in network.androidFiles)
                {
                    files.Add(file);
                }
            }
        }

        return files;
    }

    [MenuItem("AATKit/Settings")]
    public static void EditSettings()
    {
        Selection.activeObject = Instance;
    }

    private static AATKitUnitySettings instance;

    public static AATKitUnitySettings Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<AATKitUnitySettings>("AATKit/AATKitSettings");
            }

            if (instance == null)
            {
                CreateNewInstance();
            }

            if (!instance.version.Equals(AATKitBinding.Version))
            {
                UpdateInstance();
            }

            return instance;
        }
    }

    private static void CreateNewInstance()
    {
        instance = CreateInstance<AATKitUnitySettings>();
        instance.networks = new List<AATKitNetworkConfiguration>(supportedNetworks);
        instance.version = AATKitBinding.Version;

        string settingsPath = "Resources/AATKit";
        string resourcesPath = Application.dataPath + "/Resources";
        string actualPath = Application.dataPath + "/" + settingsPath;

        if (!Directory.Exists(resourcesPath))
        {
            AssetDatabase.CreateFolder("Assets", "Resources");
        }

        if (!Directory.Exists(actualPath))
        {
            AssetDatabase.CreateFolder("Assets/Resources", "AATKit");
        }

        string fullPath = "Assets/" + settingsPath + "/AATKitSettings.asset";

        AssetDatabase.CreateAsset(instance, fullPath);
    }

    private static void UpdateInstance()
    {
        RemoveUnsupportedNetworks();
        UpdateNetworks();
        AddNewSupportedNetworks();
        instance.networks = instance.networks.OrderBy(x => x.networkName).ToList();
        instance.version = AATKitBinding.Version;
    }

    private static void RemoveUnsupportedNetworks()
    {
        for (int i = instance.networks.Count - 1; i >= 0; i--)
        {
            AATKitNetworkConfiguration network = instance.networks[i];

            if (!supportedNetworks.Any(x => x.networkName == network.networkName))
            {
                instance.networks.RemoveAt(i);
            }
        }
    }

    private static void UpdateNetworks()
    {
        foreach (AATKitNetworkConfiguration instanceNetwork in instance.networks)
        {
            AATKitNetworkConfiguration supportedNetwork = supportedNetworks.FindLast(x => x.networkName == instanceNetwork.networkName);
            instanceNetwork.androidNetwork = supportedNetwork.androidNetwork;
            instanceNetwork.androidFiles = supportedNetwork.androidFiles;
            instanceNetwork.iOSNetwork = supportedNetwork.iOSNetwork;
            instanceNetwork.iOSDirectories = supportedNetwork.iOSDirectories;
        }
    }

    private static void AddNewSupportedNetworks()
    {
        foreach (AATKitNetworkConfiguration network in supportedNetworks)
        {
            if (!instance.networks.Any(x => x.networkName == network.networkName))
            {
                instance.networks.Add(network);
            }
        }
    }
}

[System.Serializable]
public class AATKitNetworkConfiguration
{
    public string networkName;

    public bool androidNetwork;

    public bool iOSNetwork;

    public string[] androidFiles;

    [Obsolete]
    public string[] iOSDirectories;

    public string[] iOSPods;

    public bool androidEnabled;

    public bool iOSEnabled;

    public AATKitNetworkConfiguration(
        string networkName,
        string[] androidFiles = null,
        string[] iOSDirectories = null,
        string[] iOSPods = null,
        bool androidNetwork = true,
        bool iOSNetwork = true,
        bool androidEnabled = true,
        bool iOSEnabled = true)
    {
        androidFiles = androidFiles ?? new string[0];
        iOSDirectories = iOSDirectories ?? new string[0];

        this.networkName = networkName;
        this.androidFiles = androidFiles;
        this.iOSDirectories = iOSDirectories;
        this.iOSPods = iOSPods;
        this.androidNetwork = androidNetwork;
        this.iOSNetwork = iOSNetwork;
        this.androidEnabled = androidEnabled;
        this.iOSEnabled = iOSEnabled;
    }
}

#endif