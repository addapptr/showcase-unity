using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class AATKitBinding : MonoBehaviour
{
    public static readonly string Version = "2.21.1";

	public enum PlacementSize
    {
        Banner320x53 = 0,
        Banner768x90 = 1,
        Banner300x250 = 2,
        Fullscreen = 3,
        MultiSizeBanner = 6,
        Rewarded = 7,
		BannerAuto = 10 // Auto choose banner type. Banner320x53 - iphone, Banner768x90 - ipad
	}

	public enum BannerAlignment
	{
		TopLeft = 0,
		TopCenter = 1,
		TopRight = 2,
		BottomLeft = 3,
		BottomCenter = 4,
		BottomRight = 5
	}

    public enum AdNetwork
    {
		ADCOLONY = 0,
		ADMOB = 1,
		ADX = 2,
		AMAZONHB = 3,
		APPLOVIN = 4,
		APPNEXUS = 5,
		CRITEOSDK = 6,
		DFP = 7,
		EMPTY = 8,
		FACEBOOK = 9,
		GENERICVAST = 10,
		HUAWEI = 11,
		INMOBI = 12,
		MOPUB = 13,
		OGURY = 14,
		PUBNATIVE = 15,
		RUBICON = 16,
		SMAATO = 17,
		SMARTAD = 18,
		SMARTADSERVERDIRECT = 19,
		SPOTX = 20,
		TEADS = 21,
		UNITYADS = 22,
		YANDEX = 23,
		BLUESTACK = 24
	}

	public enum NativeAdType
	{
		APP_INSTALL = 0,
		CONTENT = 1,
		OTHER = 2,
		UNKNOWN = 3,
		VIDEO = 4
	};

	public enum PlacementContentGravity
	{
		Top = 0,
		Bottom = 1,
		Center = 2
	};

	public enum ManagedConsentState
    {
		Unknown,
		Withheld,
		Custom,
		Obtained
	}

	private static readonly string LogPrefix = "[AATKitBinding] ";

    private static readonly string CSVSeparator = ";";

	private static readonly string NewLine = "\n";

//-----------------------------------------------------------------------------------------------------
    private static AATKitBinding _instance = null;
	public static AATKitBinding Instance
	{
		get { return _instance; }
	}

	public static Action<string> OnHaveAdDelegate;

	public static Action<string> OnHaveAdOnMultiSizeBannerDelegate;

	public static Action<string> OnNoAdDelegate;

	public static Action<string> OnPauseForAdDelegate;

	public static Action<string> OnResumeAfterAdDelegate;

	public static Action<string> OnShowingEmptyDelegate;

	public static Action<AATKitIncentiveData> OnUserEarnedIncentiveDelegate;

	public static Action<bool> OnObtainedAdRulesDelegate;

	public static Action OnManagedConsentNeedsUserInterfaceDelegate;

	public static Action<ManagedConsentState> OnManagedConsentCMPFinishedDelegate;

	public static Action<string> OnManagedConsentCMPFailedToLoadDelegate;

	public static Action<string> OnManagedConsentCMPFailedToShowDelegate;

	public static bool ScriptLogEnabled
    {
        private get;
        set;
    }

    private static void Log(string message)
    {
        if(ScriptLogEnabled)
        {
            Debug.Log(LogPrefix + message);
        }
    }

	void Awake()
	{
		ScriptLogEnabled = true;
		Log("Awake");

        if (_instance != null && _instance != this)
		{
			Destroy(gameObject);
			return;
		}
		
		_instance = this;
		
		DontDestroyOnLoad(gameObject); // Don't get destroyed when loading a new level.

        #if UNITY_ANDROID
        if (_plugin == null && Application.platform == RuntimePlatform.Android)
        {
            using (AndroidJavaClass pluginClass = new AndroidJavaClass("com.intentsoftware.addapptr.AATKitActivity"))
            {
                _plugin = pluginClass.CallStatic<AndroidJavaObject> ("instance");
            }
        }
        #endif
	}

	//===============================================================================================================================================================
	// interface to objective-c runtime (internal use only)
	#region Externals
#if UNITY_IOS

    [DllImport("__Internal")]
    private static extern void aatkitInitWithConfiguration(string objectName, string configuration);

    [DllImport("__Internal")]
    private static extern void aatkitReconfigureUsingConfiguration(string configuration);

	[DllImport("__Internal")]
	private static extern void aatkitSetDebugEnabled();

	[DllImport("__Internal")]
	private static extern void aatkitSetDebugShakeEnabled(bool enabled);

	[DllImport("__Internal")]
	private static extern string aatkitGetVersion();

	[DllImport("__Internal")]
	private static extern string aatkitGetDebugInfo();

	[DllImport("__Internal")]
	private static extern bool aatkitIsNetworkEnabled(int network);

	[DllImport("__Internal")]
	private static extern void aatkitSetNetworkEnabled(int network, bool enabled);

	[DllImport("__Internal")]
	private static extern string aatkitCreatePlacement(string placementName, int placementSize);

	[DllImport("__Internal")]
	private static extern void aatkitAddPlacementToView(string placementName);

	[DllImport("__Internal")]
	private static extern void aatkitRemovePlacementFromView(string placementName);

	[DllImport("__Internal")]
	private static extern void aatkitStartPlacementAutoReload(string placementName);
	
	[DllImport("__Internal")]
	private static extern void aatkitStopPlacementAutoReload(string placementName);
	
	[DllImport("__Internal")]
	private static extern bool aatkitReloadPlacement(string placementName);
	
	[DllImport("__Internal")]
	private static extern bool aatkitReloadPlacementForced(string placementName, bool forced);

	[DllImport("__Internal")]
	private static extern bool aatkitHasAdForPlacement(string placementName);
	
	[DllImport("__Internal")]
	private static extern void aatkitStartPlacementAutoReloadWithSeconds(string placementName, int seconds);

    [DllImport("__Internal")]
    private static extern void aatkitSetPlacementAlignment(string placementName, int bannerAlignment);

	[DllImport("__Internal")]
	private static extern void aatkitSetPlacementAlignmentWithOffsetInPixels(string placementName, int bannerAlignment, int x, int y);

	[DllImport("__Internal")]
    private static extern void aatkitSetPlacementAlignmentWithOffset(string placementName, int bannerAlignment, int x, int y);

    [DllImport("__Internal")]
    private static extern void aatkitSetMultiSizeAlignment(string placementName, int bannerAlignment);

    [DllImport("__Internal")]
    private static extern void aatkitSetMultiSizeAlignmentWithOffset(string placementName, int bannerAlignment, int x, int y);

	[DllImport("__Internal")]
	private static extern void aatkitSetPlacementPositionInPixels(string placementName, int posX, int posY);

	[DllImport("__Internal")]
	private static extern void aatkitSetPlacementPosition(string placementName, int posX, int posY);

	[DllImport("__Internal")]
	private static extern void aatkitSetMultiSizePosition(string placementName, int posX, int posY);
	
	[DllImport("__Internal")]
	private static extern bool aatkitShowPlacement(string placementName);

    [DllImport("__Internal")]
    private static extern bool aatkitIsFrequencyCapReachedForPlacement(string placementName);

	[DllImport("__Internal")]
	private static extern void aatkitSetPlacementContentGravity(string placementName, int gravity);

	[DllImport("__Internal")]
	private static extern void aatkitSetTargetingInfo (string info);

	[DllImport("__Internal")]
	private static extern void aatkitSetTargetingInfoForPlacement (string placementName, string info);

	[DllImport("__Internal")]
	private static extern void aatkitSetContentTargetingUrl (string url);

	[DllImport("__Internal")]
	private static extern void aatkitSetContentTargetingUrlForPlacement (string placementName, string url);

	[DllImport("__Internal")]
	private static extern void aatkitAddAdNetworkForKeywordTargeting (int network);

	[DllImport("__Internal")]
	private static extern void aatkitRemoveAdNetworkForKeywordTargeting (int network);

    [DllImport("__Internal")]
    private static extern bool aatkitShowConsentDialogIfNeeded();

	[DllImport("__Internal")]
	private static extern void aatkitEditConsent();

	[DllImport("__Internal")]
	private static extern void aatkitReloadConsent();

	[DllImport("__Internal")]
    private static extern float aatkitGetScale();

    [DllImport("__Internal")]
    private static extern float aatkitGetNativeScale();

#endif
	#endregion

#if UNITY_ANDROID
	private static AndroidJavaObject _plugin;
#endif

    //===============================================================================================================================================================
#if UNITY_ANDROID
	void OnApplicationPause(bool pause)
	{
        Log("OnApplicationPause pause: " + pause);

		if(Application.platform == RuntimePlatform.Android && _plugin != null)
		{
			if(pause)
			{
				_plugin.Call("aatkitOnPause");
			}
			else
			{
				_plugin.Call("aatkitOnResume");
			}
		}
	}

	void OnApplicationQuit()
	{
        Log("OnApplicationQuit");
		if(Application.platform == RuntimePlatform.Android && _plugin != null)
		{
			_plugin.Call("aatkitOnDestroy");
		}
	}
#endif

	public static void Init(AATKitConfiguration configuration)
    {
		string objectName = _instance.gameObject.name;
#pragma warning disable CS0612 // This method is allowed to be used intenrally.
        Init(objectName, configuration);
#pragma warning restore CS0612
    }

	[Obsolete]
	public static void Init(string objectName, AATKitConfiguration configuration)
    {
        string jsonConfiguration = JsonUtility.ToJson(configuration);
        Log("Init objectName: " + objectName + " configuration: " + jsonConfiguration);

#if UNITY_IOS
        if(IsRunningOnIOSDevice())
        {
            aatkitInitWithConfiguration(objectName, jsonConfiguration);
        }
#elif UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
        {
            if (_plugin != null)
            {
                _plugin.Call("aatkitInit", objectName, jsonConfiguration);
            }
        }
#endif

        if (Application.isEditor && AATKitPlaceholderManager.Instance != null)
        {
            AATKitPlaceholderManager.Instance.Initialize(objectName);
        }
    }

    public static void ReconfigureUsingConfiguration(AATKitConfiguration configuration)
    {
        string jsonConfiguration = JsonUtility.ToJson(configuration);
        Log("ReconfigureUsingConfiguration configuration: " + jsonConfiguration);

#if UNITY_IOS
        if (IsRunningOnIOSDevice())
        {
            aatkitReconfigureUsingConfiguration(jsonConfiguration);
        }
#elif UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
        {
            if (_plugin != null)
            {
                _plugin.Call("aatkitReconfigureUsingConfiguration", jsonConfiguration);
            }
        }
#endif
    }

    //===============================================================================================================================================================

	// Enables debug mode.
	public static void SetDebugEnabled()
	{
        Log("SetDebugEnabled");

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitSetDebugEnabled();
		}
#elif UNITY_ANDROID
		if (Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitSetDebugEnabled");
			}
		}
#endif
	}

	// Enables/disables debug screen that will show after shaking the device. Only to be used for testing during development.
	public static void SetDebugShakeEnabled(bool enabled)
	{
        Log("SetDebugShakeEnabled enabled: " + enabled);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitSetDebugShakeEnabled(enabled);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitSetDebugShakeEnabled", enabled);
			}
		}
#endif
	}

	// AATKit version in X.Y.Z(internal XXYY) format. For example: 1.0.1 (internal 0243).
	public static string GetVersion()
	{
        Log("GetVersion");

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			return aatkitGetVersion();
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				return _plugin.Call<string>("aatkitGetVersion");
			}
		}
#endif

		return "unknown";
	}

	// Used for obtaining debug information (the same that would be presented in dialog after shaking the device if debug screen is enabled)
	public static string GetDebugInfo()
	{
        Log("GetDebugInfo");

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			return aatkitGetDebugInfo();
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				return _plugin.Call<string>("aatkitGetDebugInfo");
			}
		}
#endif

		return "unknown";
	}

	// Checks if ad network is enabled.
	public static bool IsNetworkEnabled(int network)
	{
        Log("IsNetworkEnabled network:" + network);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			return aatkitIsNetworkEnabled((int)network);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				return _plugin.Call<bool>("aatkitIsNetworkEnable", (int)network);
			}
		}
#endif

		return false;
	}
		
	// Allows to enable or disable selected ad networks. By default all networks are enabled.
	public static void SetNetworkEnabled(int network, bool enabled)
	{
        Log("SetNetworkEnabled network:" + network + " enabled: " + enabled);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitSetNetworkEnabled((int)network, enabled);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitSetNetworkEnabled", (int)network, enabled);
			}
		}
#endif
	}

	// Creates placement with given name and size.
	public static Vector2 CreatePlacement(string placementName, PlacementSize placementSize)
	{
        Log("CreatePlacement placementName:" + placementName + " placementSize: " + placementSize);

        string result = "0x0";
		int x, y;

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			result = aatkitCreatePlacement(placementName, (int)placementSize);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				result = _plugin.Call<string>("aatkitCreatePlacement", placementName, (int)placementSize);
			}
		}
#endif

        if(Application.isEditor && AATKitPlaceholderManager.Instance != null)
        {
            AATKitPlaceholderManager.Instance.CreatePlacement(placementName, placementSize);
        }

		char[] delimiterChars = {'x'};
		string[] pos = result.Split(delimiterChars);

		try
		{
			x = int.Parse(pos[0]);
			y = int.Parse(pos[1]);
			
		}
		catch (Exception e)
		{
			Debug.LogException(e);
			x = 0;
			y = 0;
		}

		return new Vector2(x, y);
	}

	// Adds placement with given name to your view.
	public static void AddPlacementToView(string placementName)
	{
        Log("AddPlacementToView placementName:" + placementName);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitAddPlacementToView(placementName);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitAddPlacementToView", placementName);
			}
		}
#endif

        if (Application.isEditor && AATKitPlaceholderManager.Instance != null)
        {
            AATKitPlaceholderManager.Instance.AddPlacementToView(placementName);
        }
    }

	// Removes placement with given name from view.
	public static void RemovePlacementFromView(string placementName)
	{
        Log("RemovePlacementFromView placementName:" + placementName);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitRemovePlacementFromView(placementName);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitRemovePlacementFromView", placementName);
			}
		}
#endif

        if (Application.isEditor && AATKitPlaceholderManager.Instance != null)
        {
            AATKitPlaceholderManager.Instance.RemovePlacementFromView(placementName);
        }
    }

	// Enables automatic reloading of placement. Autoreloader will use reload time configured on addapptr.com account or fallback to default 30L seconds.
	public static void StartPlacementAutoReload(string placementName)
	{
        Log("StartPlacementAutoReload placementName:" + placementName);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitStartPlacementAutoReload(placementName);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitStartPlacementAutoReload", placementName);
			}
		}
#endif

        if (Application.isEditor && AATKitPlaceholderManager.Instance != null)
        {
            AATKitPlaceholderManager.Instance.StartPlacementAutoReload(placementName);
        }
    }

	// Disables automatic reloading of placement.
	public static void StopPlacementAutoReload(string placementName)
	{
        Log("StopPlacementAutoReload placementName:" + placementName);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitStopPlacementAutoReload(placementName);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitStopPlacementAutoReload", placementName);
			}
		}
#endif

        if (Application.isEditor && AATKitPlaceholderManager.Instance != null)
        {
            AATKitPlaceholderManager.Instance.StopPlacementAutoReload(placementName);
        }
    }

	// Requests placement reload. Works only if automatic reloading is disabled.
	public static bool ReloadPlacement(string placementName)
	{
        Log("ReloadPlacement placementName:" + placementName);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			return aatkitReloadPlacement(placementName);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				return _plugin.Call<bool>("aatkitReloadPlacement", placementName);
			}
		}
#endif

        if (Application.isEditor && AATKitPlaceholderManager.Instance != null)
        {
            AATKitPlaceholderManager.Instance.ReloadPlacement(placementName);
            return true;
        }

        return false;
	}
	
	// Requests placement reload. Works only if automatic reloading is disabled.
	public static bool ReloadPlacementForced(string placementName, bool forced)
	{
        Log("ReloadPlacementForced placementName:" + placementName + " forced: " + forced);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			return aatkitReloadPlacementForced(placementName, forced);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				return _plugin.Call<bool>("aatkitReloadPlacementForced", placementName, forced);
			}
		}
#endif

        if (Application.isEditor && AATKitPlaceholderManager.Instance != null)
        {
            AATKitPlaceholderManager.Instance.ReloadPlacement(placementName);
            return true;
        }

        return false;
	}

	// Returns true if there is an ad loaded for given placementId.
	public static bool HasAdForPlacement(string placementName)
	{
        Log("HasAdForPlacement placementName:" + placementName);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			return aatkitHasAdForPlacement(placementName);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				return _plugin.Call<bool>("aatkitHasAdForPlacement", placementName);
			}
		}
#endif

		return false;
	}

	// Enables automatic reloading of placement and sets custom reload time. This reload time will be used instead of time configured on addapptr.com account.
	public static void StartPlacementAutoReloadWithSeconds(string placementName, int seconds)
	{
        Log("StartPlacementAutoReloadWithSeconds placementName:" + placementName + " seconds: " + seconds);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitStartPlacementAutoReloadWithSeconds(placementName, seconds);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitStartPlacementAutoReloadWithSeconds", placementName, seconds);
			}
		}
#endif

        if (Application.isEditor && AATKitPlaceholderManager.Instance != null)
        {
            AATKitPlaceholderManager.Instance.StartPlacementAutoReload(placementName);
        }
    }

	// Sets multi size banner placement position on the screen.
    public static void SetMultiSizeAlignment(string placementName, BannerAlignment multiSizeAlignment)
    {
        Log("SetMultiSizeAlignment placementName:" + placementName + " multiSizeAlignment: " + multiSizeAlignment);

#if UNITY_IOS
        if(IsRunningOnIOSDevice())
        {
            aatkitSetMultiSizeAlignment(placementName, (int)multiSizeAlignment);
        }
#elif UNITY_ANDROID
        if(Application.platform == RuntimePlatform.Android)
        {
            if(_plugin != null)
            {
                _plugin.Call("aatkitSetMultiSizeAlignment", placementName, (int)multiSizeAlignment);
            }
        }
#endif
    }

    public static void SetMultiSizeAlignmentWithOffset(string placementName, BannerAlignment multiSizeAlignment, int x, int y)
    {
        Log("SetMultiSizeAlignmentWithOffset placementName:" + placementName + " multiSizeAlignment: " + multiSizeAlignment + " x: " + x + " y: " + y);

#if UNITY_IOS
        if (IsRunningOnIOSDevice())
        {
            aatkitSetMultiSizeAlignmentWithOffset(placementName, (int)multiSizeAlignment, x, y);
        }
#elif UNITY_ANDROID
        if(Application.platform == RuntimePlatform.Android)
        {
            if(_plugin != null)
            {
                _plugin.Call("aatkitSetMultiSizeAlignmentWithOffset", placementName, (int)multiSizeAlignment, x, y);
            }
        }
#endif
    }

    // Sets banner placement position on the screen.
    public static void SetPlacementAlignment(string placementName, BannerAlignment bannerAlignment)
    {
        Log("SetPlacementAlignment placementName:" + placementName + " bannerAlignment: " + bannerAlignment);

#if UNITY_IOS
        if(IsRunningOnIOSDevice())
        {
            aatkitSetPlacementAlignment(placementName, (int)bannerAlignment);
        }
#elif UNITY_ANDROID
        if(Application.platform == RuntimePlatform.Android)
        {
            if(_plugin != null)
            {
                _plugin.Call("aatkitSetPlacementAlignment", placementName, (int)bannerAlignment);
            }
        }
#endif

        if (Application.isEditor && AATKitPlaceholderManager.Instance != null)
        {
            AATKitPlaceholderManager.Instance.SetPlacementAlignment(placementName, bannerAlignment);
        }
    }

	public static void SetPlacementAlignmentWithOffsetInPixels(string placementName, BannerAlignment bannerAlignment, int x, int y)
	{
		Log("SetPlacementAlignmentWithOffsetInPixels placementName:" + placementName + " bannerAlignment: " + bannerAlignment + " x: " + x + " y:" + y);

#if UNITY_IOS
		if (IsRunningOnIOSDevice())
		{
			aatkitSetPlacementAlignmentWithOffsetInPixels(placementName, (int)bannerAlignment, x, y);
		}
#elif UNITY_ANDROID
        if(Application.platform == RuntimePlatform.Android)
        {
            if(_plugin != null)
            {
                _plugin.Call("aatkitSetPlacementAlignmentWithOffsetInPixels", placementName, (int)bannerAlignment, x, y);
            }
        }
#endif
	}

	public static void SetPlacementAlignmentWithOffset(string placementName, BannerAlignment bannerAlignment, int x, int y)
    {
        Log("SetPlacementAlignmentWithOffset placementName:" + placementName + " bannerAlignment: " + bannerAlignment + " x: " + x + " y:" + y);

#if UNITY_IOS
        if (IsRunningOnIOSDevice())
        {
            aatkitSetPlacementAlignmentWithOffset(placementName, (int)bannerAlignment, x, y);
        }
#elif UNITY_ANDROID
        if(Application.platform == RuntimePlatform.Android)
        {
            if(_plugin != null)
            {
                _plugin.Call("aatkitSetPlacementAlignmentWithOffset", placementName, (int)bannerAlignment, x, y);
            }
        }
#endif
    }

	// Sets banner placement position on the screen in pixels (x, y)
	public static void SetPlacementPositionInPixels(string placementName, int posX, int posY)
	{
		Log("SetPlacementPositionInPixels placementName:" + placementName + " posX: " + posX + " posY: " + posY);

#if UNITY_IOS
		if (IsRunningOnIOSDevice())
		{
			aatkitSetPlacementPositionInPixels(placementName, posX, posY);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitSetPlacementPositionInPixels", placementName, posX, posY);
			}
		}
#endif
	}

	// Sets banner placement position on the screen (x, y)
	public static void SetPlacementPosition(string placementName, int posX, int posY)
	{
        Log("SetPlacementPosition placementName:" + placementName + " posX: " + posX + " posY: " + posY);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitSetPlacementPosition(placementName, posX, posY);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitSetPlacementPosition", placementName, posX, posY);
			}
		}
#endif
    }

	// Sets multi size banner placement position on the screen (x, y)
	public static void SetMultiSizePosition(String placementName, int posX, int posY)
	{
        Log("SetMultiSizePosition placementName:" + placementName + " posX: " + posX + " posY: " + posY);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitSetMultiSizePosition(placementName, posX, posY);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitSetMultiSizePosition", placementName, posX, posY);
			}
		}
#endif
	}

	// Shows interstitial ad if ad is ready.
	public static bool ShowPlacement(string placementName)
	{
        Log("ShowPlacement placementName:" + placementName);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			return aatkitShowPlacement(placementName);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				return _plugin.Call<bool>("aatkitShowPlacement", placementName);
			}
		}
#endif

        if (Application.isEditor && AATKitPlaceholderManager.Instance != null)
        {
            return AATKitPlaceholderManager.Instance.ShowPlacement(placementName);
        }

        return false;
	}

    public static bool IsFrequencyCapReachedForPlacement(string placementName)
    {
        Log("IsFrequencyCapReachedForPlacement placementName:" + placementName);

#if UNITY_IOS
        if(IsRunningOnIOSDevice())
        {
            return aatkitIsFrequencyCapReachedForPlacement(placementName);
        }
#elif UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
        {
            if (_plugin != null)
            {
                return _plugin.Call<bool>("aatkitIsFrequencyCapReachedForPlacement", placementName);
            }
        }
#endif

        return false;
    }
		

	public static void AddAdNetworkForKeywordTargeting(AdNetwork network)
	{
        Log("AddAdNetworkForKeywordTargeting network: " + network);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitAddAdNetworkForKeywordTargeting((int) network);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitAddAdNetworkForKeywordTargeting", (int) network);
			}
		}
#endif
	}

	public static void RemoveAdNetworkForKeywordTargeting(AdNetwork network)
	{
        Log("RemoveAdNetworkForKeywordTargeting network: " + network);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitRemoveAdNetworkForKeywordTargeting((int) network);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitRemoveAdNetworkForKeywordTargeting", (int) network);
			}
		}
#endif
	}

	public static void SetTargetingInfo(String placementName, Dictionary<string, List<string>> info)
	{
        Log("SetTargetingInfo placementName: " + placementName + " info: " + CreateCSVForTargetInfo(info));

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitSetTargetingInfoForPlacement(placementName, CreateCSVForTargetInfo(info));
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitSetTargetingInfo", placementName, info);
			}
		}
#endif
	}

	public static void SetTargetingInfo(Dictionary<String, List<string>> info)
	{
		Log("SetTargetingInfo info: " + CreateCSVForTargetInfo(info));
#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitSetTargetingInfo(CreateCSVForTargetInfo(info));
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitSetTargetingInfo", CreateAndroidTargetInfo(info));
			}
		}
#endif
	}

	public static void SetContentTargetingUrl(string url)
	{
		Log("SetContentTargetingUrl url: " + url);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitSetContentTargetingUrl(url);
		}
#elif UNITY_ANDROID
		if (Application.platform == RuntimePlatform.Android)
		{
			if (_plugin != null)
			{
				_plugin.Call("aatkitSetContentTargetingUrl", url);
			}
		}
#endif
	}

	public static void SetContentTargetingUrl(string placementName, string url)
	{
		Log("SetContentTargetingUrl placementName: " + placementName + " url: " + url);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitSetContentTargetingUrlForPlacement(placementName, url);
		}
#elif UNITY_ANDROID
		if (Application.platform == RuntimePlatform.Android)
		{
			if (_plugin != null)
			{
				_plugin.Call("aatkitSetContentTargetingUrl", placementName, url);
			}
		}
#endif
	}

	private static string CreateCSVForTargetInfo(Dictionary<String, List<string>> info)
	{
		string result = "";
		int lineNumber = 0;

		foreach (KeyValuePair<string, List<string>> entry in info) 
		{
			if (lineNumber != 0) 
			{
				result += NewLine;
			}
			result += entry.Key;
			lineNumber++;

			List<string> values = entry.Value;
			for(int i=0; i<values.Count; i++)
			{
				result += CSVSeparator + values [i];
			}
		}

		return result;
	}

	private static AndroidJavaObject CreateAndroidTargetInfo(Dictionary<string, List<string>> info)
	{
		AndroidJavaObject map = new AndroidJavaObject ("java.util.HashMap");

		foreach(KeyValuePair<string, List<string>> entry in info)
		{
			AndroidJavaObject list = new AndroidJavaObject("java.util.LinkedList");
			foreach (String infoValue in entry.Value) 
			{
				AndroidJavaObject infoValueString = new AndroidJavaObject ("java.lang.String", infoValue);
				list.Call<bool>("add", infoValueString);
			}


			AndroidJavaObject keyString = new AndroidJavaObject ("java.lang.String", entry.Key);
			IntPtr mapMethodPut = AndroidJNIHelper.GetMethodID(map.GetRawClass(), "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");

			object[] args = new object[2];
			args[0] = keyString;
			args[1] = list;

			AndroidJNI.CallObjectMethod(map.GetRawObject(), mapMethodPut, AndroidJNIHelper.CreateJNIArgArray(args));
		}

		return map;
	}

	public static void SetPlacementContentGravity(string placementName, PlacementContentGravity gravity)
	{
        Log("SetPlacementContentGravity placementName: " + placementName + " gravity: " + gravity);

#if UNITY_IOS
		if(IsRunningOnIOSDevice())
		{
			aatkitSetPlacementContentGravity(placementName, (int)gravity);
		}
#elif UNITY_ANDROID
		if(Application.platform == RuntimePlatform.Android)
		{
			if(_plugin != null)
			{
				_plugin.Call("aatkitSetPlacementContentGravity", placementName, (int)gravity);
			}
		}
#endif
	}

    public static bool ShowConsentDialogIfNeeded()
    {
        Log("ShowConsentDialogIfNeeded");

#if UNITY_IOS
        if(IsRunningOnIOSDevice())
        {
            return aatkitShowConsentDialogIfNeeded();
        }
#elif UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
        {
            if (_plugin != null)
            {
                return _plugin.Call<bool>("aatkitShowConsentDialogIfNeeded");
            }
        }
#endif

        return false;
    }

	public static void EditConsent()
	{
		Log("EditConsentDialog");

#if UNITY_IOS
		if (IsRunningOnIOSDevice())
		{
			aatkitEditConsent();
		}
#elif UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
        {
            if (_plugin != null)
            {
                _plugin.Call("aatkitEditConsentDialog");
            }
        }
#endif
	}

	public static void ReloadConsent()
	{
		Log("ReloadConsent");

#if UNITY_IOS
		if (IsRunningOnIOSDevice())
		{
			aatkitReloadConsent();
		}
#elif UNITY_ANDROID
        if (Application.platform == RuntimePlatform.Android)
        {
            if (_plugin != null)
            {
                _plugin.Call("aatkitReloadConsent");
            }
        }
#endif
	}

	public static float GetScale()
    {
        Log("GetScale");

#if UNITY_IOS
        if (IsRunningOnIOSDevice())
        {
            return aatkitGetScale();
        }
#endif

        return 0;
    }

    public static float GetNativeScale()
    {
        Log("GetNativeScale");

#if UNITY_IOS
        if (IsRunningOnIOSDevice())
        {
            return aatkitGetNativeScale();
        }
#endif

        return 0;
    }

	public void OnHaveAd(string placementName)
    {
        Log("OnHaveAd placementName: " + placementName);
        CallDelegate(placementName, OnHaveAdDelegate);
    }

    public void OnHaveAdOnMultiSizeBanner(string placementName)
	{
		Log("OnHaveAdOnMultiSizeBanner placementName: " + placementName);
		CallDelegate(placementName, OnHaveAdOnMultiSizeBannerDelegate);
	}

	public void OnNoAd(string placementName)
	{
		Log("OnNoAd placementName: " + placementName);
		CallDelegate(placementName, OnNoAdDelegate);
	}

	public void OnPauseForAd(string placementName)
	{
		Log("OnPauseForAd placementName: " + placementName);
		CallDelegate(placementName, OnPauseForAdDelegate);
	}

	public void OnResumeAfterAd(string placementName)
	{
		Log("OnResumeAfterAd placementName: " + placementName);
		CallDelegate(placementName, OnResumeAfterAdDelegate);
	}

	public void OnShowingEmpty(string placementName)
	{
		Log("OnShowingEmpty placementName: " + placementName);
		CallDelegate(placementName, OnShowingEmptyDelegate);
	}

	public void OnUserEarnedIncentive(string incentiveDataJson)
	{
		Log("OnUserEarnedIncentive event: " + incentiveDataJson);
		AATKitIncentiveData incentiveData = JsonUtility.FromJson<AATKitIncentiveData>(incentiveDataJson);
		CallDelegate(incentiveData, OnUserEarnedIncentiveDelegate);
	}

	public void OnObtainedAdRules(string fromTheServerString)
	{
		Log("OnObtainedAdRules fromTheServer: " + fromTheServerString);
		bool fromTheServer = fromTheServerString == "true";
		CallDelegate(fromTheServer, OnObtainedAdRulesDelegate);
	}

	public void OnManagedConsentNeedsUserInterface()
	{
		Log("OnManagedConsentNeedsUserInterface");
		CallDelegate(OnManagedConsentNeedsUserInterfaceDelegate);
	}

	public void OnManagedConsentCMPFinished(string stateString)
	{
		Log("OnManagedConsentCMPFinished state: " + stateString);
		ManagedConsentState state = ManagedConsentState.Unknown;
		switch(stateString)
        {
			case "Withheld":
				state = ManagedConsentState.Withheld;
				break;
			case "Custom":
				state = ManagedConsentState.Custom;
				break;
			case "Obtained":
				state = ManagedConsentState.Obtained;
				break;
		}
		CallDelegate(state, OnManagedConsentCMPFinishedDelegate);
	}

	public void OnManagedConsentCMPFailedToLoad(string error)
	{
		Log("OnManagedConsentCMPFailedToLoad error: " + error);
		CallDelegate(error, OnManagedConsentCMPFailedToLoadDelegate);
	}

	public void OnManagedConsentCMPFailedToShow(string error)
	{
		Log("OnManagedConsentCMPFailedToShow error: " + error);
		CallDelegate(error, OnManagedConsentCMPFailedToShowDelegate);
	}

	private static void CallDelegate(Action del)
	{
		if (del != null)
		{
			Log("Calling delegate.");
			del.Invoke();
		}
		else
		{
			Log("Not calling delegate. Delegate is null.");
		}
	}

	private static void CallDelegate<T>(T value, Action<T> del)
	{
		if (del != null)
		{
			Log("Calling delegate.");
			del.Invoke(value);
		}
		else
		{
			Log("Not calling delegate. Delegate is null.");
		}
	}

	private static bool IsRunningOnIOSDevice()
	{
		return Application.platform == RuntimePlatform.IPhonePlayer && Application.installMode != ApplicationInstallMode.Editor;
	}
}
