using System;
using System.Collections.Generic;

[Serializable]
public class AATKitConfiguration
{
    public enum PlatformType
    {
        Android,
        Huawei
    }

    public string AlternativeBundleId = string.Empty;

    public bool ConsentRequired = true;

    public string InitialRules = string.Empty;

    public bool ShouldCacheRules = true;

    public bool ShouldSkipRules = false;

    public bool ShouldReportUsingAlternativeBundleId = true;

    public int TestModeAccountId = 0;

    public bool UseDebugShake = true;

    public bool UseGeoLocation = false;

    public PlatformType Platform = PlatformType.Android;

    public AATKitConsent Consent = new AATKitConsent();
}