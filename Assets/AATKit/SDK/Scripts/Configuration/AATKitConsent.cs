﻿using System;
using System.Collections.Generic;

[Serializable]
public class AATKitConsent
{
    public enum ConsentTypes
    {
        ManagedCMPGoogle,
        ManagedCMPOgury,
        Simple,
        Vendor,
        None
    }

    public enum NonIABConsentType
    {
        Obtained,
        Unknown,
        Withheld
    }

    public ConsentTypes Type = ConsentTypes.None;

    public NonIABConsentType NonIABConsent = NonIABConsentType.Unknown;

    public List<AATKitBinding.AdNetwork> VendorConsentObtainedNetworks = new List<AATKitBinding.AdNetwork>();

    public List<AATKitBinding.AdNetwork> NoConsentNetworkStopSet = new List<AATKitBinding.AdNetwork>();

    public NonIABConsentType ConsentForAddapptr = NonIABConsentType.Unknown;

    public string AssetKeyCMPOgury = string.Empty;
}
