﻿using System;

[Serializable]
public class AATKitIncentiveData
{
    public string PlacementName = string.Empty;

    public AATKitReward Reward = null;

    public override string ToString()
    {
        string result = "PlacementName: " + PlacementName;
        if(Reward != null)
        {
            result += " Reward.Name: " + Reward.Name + " Reward.Value: " + Reward.Value;
        }
        return result;
    }
}
