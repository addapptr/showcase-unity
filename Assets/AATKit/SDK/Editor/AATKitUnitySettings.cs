﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AATKitUnitySettings))]
public class AATKitUnitySettingsEditor : Editor
{
    public override void OnInspectorGUI()
    {
        AATKitUnitySettings settings = target as AATKitUnitySettings;
        PrintNetworksConfiguration(settings);
        PrintGoogleCMPConfiguration(settings);
    }

    private static void PrintNetworksConfiguration(AATKitUnitySettings settings)
    {
        EditorGUILayout.LabelField("Configure which libraries should be included to the build.");

        var win = EditorGUIUtility.currentViewWidth;
        var w1 = win * 0.6f;
        var w2 = win * 0.2f;
        var w3 = win * 0.2f;

        PrintTableHeader(w1, w2, w3);
        PrintNetworksRows(settings, w1, w2, w3);
    }

    private static void PrintTableHeader(float w1, float w2, float w3)
    {
        EditorGUILayout.BeginHorizontal();

        var origFontStyle = EditorStyles.label.fontStyle;
        EditorStyles.label.fontStyle = FontStyle.Bold;

        EditorGUILayout.LabelField("Network name", GUILayout.Width(w1));
        EditorGUILayout.LabelField("Android", GUILayout.Width(w2));
        EditorGUILayout.LabelField("iOS", GUILayout.Width(w3));

        EditorStyles.label.fontStyle = origFontStyle;

        EditorGUILayout.EndHorizontal();
    }

    private static void PrintNetworksRows(AATKitUnitySettings settings, float w1, float w2, float w3)
    {
        foreach (AATKitNetworkConfiguration network in settings.networks)
        {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(network.networkName, GUILayout.Width(w1));

            if (network.androidNetwork)
            {
                EditorGUI.BeginDisabledGroup(network.androidFiles.Length == 0);
                network.androidEnabled = EditorGUILayout.Toggle(network.androidEnabled, GUILayout.Width(w2));
                EditorGUI.EndDisabledGroup();

            }
            else
            {
                EditorGUILayout.LabelField("-", GUILayout.Width(w2));
            }

            if (network.iOSNetwork)
            {
                EditorGUI.BeginDisabledGroup(network.iOSPods.Length == 0);
                network.iOSEnabled = EditorGUILayout.Toggle(network.iOSEnabled, GUILayout.Width(w3));
                EditorGUI.EndDisabledGroup();
            }
            else
            {
                EditorGUILayout.LabelField("-", GUILayout.Width(w3));

            }

            EditorGUILayout.EndHorizontal();
        }
    }

    private static void PrintGoogleCMPConfiguration(AATKitUnitySettings settings)
    {
        EditorGUILayout.Separator();
        EditorGUILayout.LabelField("GADApplicationIdentifier for CMP Google (iOS)");
        settings.gadApplicationIdentifier = EditorGUILayout.TextField(settings.gadApplicationIdentifier);
    }

    private void OnDisable()
    {
        if (target != null)
        {
            EditorUtility.SetDirty(target);
        }
    }
}