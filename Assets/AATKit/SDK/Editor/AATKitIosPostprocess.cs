﻿#if UNITY_IOS

using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using UnityEditor.iOS.Xcode.Extensions;
using System.Text;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AATKitIosPostprocess : Editor
{
    private static readonly string LibPath = "usr/lib/";

    private static readonly string Podfile = "/Podfile";

    private static readonly string SwiftEmptyFileSourceRelativePath = "/AATKit/SDK/Editor/iOS/Empty.swift";

    private static readonly string SwiftEmptyFrameworkFileSourceRelativePath = "/AATKit/SDK/Editor/iOS/EmptyFramework.swift";

    private static readonly string SwiftEmptyFileDestinatonRelativePath = "Classes/Empty.swift";

    private static readonly string SwiftEmptyFrameworkFileDestinatonRelativePath = "UnityFramework/EmptyFramework.swift";

    private static PBXProject proj;

    private static string targetGuidFramework = null;

    private static string targetGuidApplication = null;

    private static string projPath;

    private static List<string> copiedFiles = new List<string>();
    
    public int callbackOrder
    {
        get
        {
            return 0;
        }
    }

    [PostProcessBuildAttribute(45)]
    private static void OnPostprocessBuildModifyPods(BuildTarget target, string buildPath)
    {
        if (target == BuildTarget.iOS)
        {
            string podfileContent = GetPodfileContainingAATKitPods(buildPath);
            SavePodfile(buildPath, podfileContent);
        }
    }

    private static string GetPodfileContainingAATKitPods(string buildPath)
    {
        UnityEngine.Debug.Log("[AATKitPostprocess] modifing the Podfile to contain AATKit pods");
        string podContent = string.Empty;

        using (var reader = new StreamReader(buildPath + Podfile))
        {
            string line;

            while ((line = reader.ReadLine()) != null)
            {
                podContent = SeparatePodsTargets(podContent, line);

                if (line.Contains("Unity-iPhone"))
                {
                    podContent = AddAATKitPods(podContent);
                }
                else if (line.Contains("UnityFramework"))
                {
                    podContent = AddAATKitPods(podContent);
                }
            }
        }

        return podContent;
    }

    private static string SeparatePodsTargets(string podContent, string line)
    {
        podContent += line + "\n";

        if (line.Equals("end"))
        {
            podContent += "\n";
        }

        return podContent;
    }

    private static string AddAATKitPods(string podContent)
    {
        foreach (string networkPod in AATKitUnitySettings.Instance.PodsForEnabledNetworks)
        {
            podContent += "  pod '" + networkPod + "'\n";
        }

        return podContent;
    }

    private static void SavePodfile(string buildPath, string podfile)
    {
        UnityEngine.Debug.Log("[AATKitPostprocess] saving modified Podfile");
        using (var writer = new StreamWriter(buildPath + Podfile))
        {
            writer.Write(podfile);
        }
    }

    [PostProcessBuildAttribute(999)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuildProject)
    {
        if (target == BuildTarget.iOS)
        {
            copiedFiles.Add("Info.plist");
            ModifyPbxProject(pathToBuildProject);
            ModifyPlist(pathToBuildProject);
            copiedFiles.Clear();
        }
    }

    static void ModifyPbxProject(string pathToBuildProject)
    {
        projPath = pathToBuildProject + "/Unity-iPhone.xcodeproj/project.pbxproj";
        proj = new PBXProject();
        proj.ReadFromString(File.ReadAllText(projPath));
#if UNITY_2019_3_OR_NEWER
        targetGuidFramework = proj.GetUnityFrameworkTargetGuid();
        targetGuidApplication = proj.GetUnityMainTargetGuid();
#else
        targetGuidFramework = proj.TargetGuidByName ("Unity-iPhone");
#endif

        AddSystemLibraries(projPath);

        UnityEngine.Debug.Log("[AATKitPostprocess] adding library search path");
        proj.AddBuildProperty(targetGuidFramework, "LIBRARY_SEARCH_PATHS", "$(PROJECT_DIR)/aat/**");

        AddLinkerFlags();

        UnityEngine.Debug.Log("[AATKitPostprocess] disabling bitode.");
        proj.SetBuildProperty(targetGuidFramework, "ENABLE_BITCODE", "false");
        if (targetGuidApplication != null)
        {
            proj.SetBuildProperty(targetGuidApplication, "ENABLE_BITCODE", "false");
        }

        UnityEngine.Debug.Log("[AATKitPostprocess] enablig workspace validation.");
        proj.SetBuildProperty(targetGuidFramework, "VALIDATE_WORKSPACE", "YES");
        if (targetGuidApplication != null)
        {
            proj.SetBuildProperty(targetGuidApplication, "VALIDATE_WORKSPACE", "YES");
        }

        if(AATKitUnitySettings.Instance.IsSwiftNeeded)
        {
            SetSwiftConfiguration(pathToBuildProject);
        }

        File.WriteAllText(projPath, proj.WriteToString());
    }

    private static void SetSwiftConfiguration(string pathToBuildProject)
    {
        UnityEngine.Debug.Log("[AATKitPostprocess] setting Swift version.");
        proj.SetBuildProperty(targetGuidFramework, "SWIFT_VERSION", "5.0");
        if (targetGuidApplication != null)
        {
            proj.SetBuildProperty(targetGuidApplication, "SWIFT_VERSION", "5.0");
        }

        AddEmptySwiftFiles(pathToBuildProject);
    }

    private static void AddLinkerFlags()
    {
        UnityEngine.Debug.Log("[AATKitPostprocess] adding linker flag - ObjC");
        proj.AddBuildProperty(targetGuidFramework, "OTHER_LDFLAGS", "-ObjC");
        if(targetGuidApplication != null)
        {
            proj.SetBuildProperty(targetGuidApplication, "OTHER_LDFLAGS", "-ObjC");
        }
    }

    static void ModifyPlist(string pathToBuildProject)
    {
        string plistPath = pathToBuildProject + "/Info.plist";
        PlistDocument plist = new PlistDocument();
        plist.ReadFromString(File.ReadAllText(plistPath));
        AddAppTransportSecurity(plist);
        ConfigurePhoneComponentsUsageDescription(plist);
        ConfigureGoogle(plist);
        File.WriteAllText(plistPath, plist.WriteToString());
    }

    static void AddSystemLibraries(string projPath)
    {
        UnityEngine.Debug.Log("[AATKitPostprocess] adding system libraries");
        StreamReader streamReader = new StreamReader("Assets/AATKit/SDK/Editor/frameworks.cfg", Encoding.Default);
        string line;
        do
        {
            line = streamReader.ReadLine();
            if (line == null)
            {
                continue;
            }
            if (line.Contains(".framework"))
            {
                proj.AddFrameworkToProject(targetGuidFramework, line, false);
            }
            else
                if (line.Contains(".dylib"))
            {
                string fileGuid = proj.AddFile(LibPath + line, "Frameworks/" + line, PBXSourceTree.Sdk);
                proj.AddFileToBuild(targetGuidFramework, fileGuid);
            }
        }
        while (line != null);
        streamReader.Close();
    }

    static void AddAppTransportSecurity(PlistDocument plist)
    {
        UnityEngine.Debug.Log("[AATKitPostprocess] adding AppTransportSecurity");
        PlistElementDict rootDict = plist.root;
        var atsKey = "NSAppTransportSecurity";
        PlistElementDict atsDict = rootDict.CreateDict(atsKey);
        var aalKey = "NSAllowsArbitraryLoads";
        atsDict.SetBoolean(aalKey, true);
    }

    static void ConfigurePhoneComponentsUsageDescription(PlistDocument plist)
    {
        PlistElementDict rootDict = plist.root;

        rootDict.SetString("NSCalendarsUsageDescription", "This App uses the Calendar to improve ads.");
        rootDict.SetString("NSCameraUsageDescription", "This App uses the Camera to improve ads.");
        rootDict.SetString("NSPhotoLibraryUsageDescription", "This App uses the Photo Library to improve ads.");
        rootDict.SetString("NSLocationAlwaysUsageDescription", "This App uses location to improve ads.");
        rootDict.SetString("NSLocationWhenInUseUsageDescription", "This App uses location to improve ads.");
    }

    static void ConfigureGoogle(PlistDocument plist)
    {
        PlistElementDict rootDict = plist.root;
        rootDict.SetBoolean("GADIsAdManagerApp", true);

        if (!string.IsNullOrEmpty(AATKitUnitySettings.Instance.gadApplicationIdentifier))
        {
            rootDict.SetString("GADApplicationIdentifier", AATKitUnitySettings.Instance.gadApplicationIdentifier);
        }
    }

    private static void AddEmptySwiftFiles(string pathToBuildProject)
    {
        UnityEngine.Debug.Log("[AATKitPostprocess] copying Empty.swift files");
        try
        {
            File.Copy(Application.dataPath + SwiftEmptyFileSourceRelativePath, pathToBuildProject + "/" + SwiftEmptyFileDestinatonRelativePath);
            string fileGuid = proj.AddFile(SwiftEmptyFileDestinatonRelativePath, SwiftEmptyFileDestinatonRelativePath, PBXSourceTree.Source);

            File.Copy(Application.dataPath + SwiftEmptyFrameworkFileSourceRelativePath, pathToBuildProject + "/" + SwiftEmptyFrameworkFileDestinatonRelativePath);
            string fileFrameworkGuid = proj.AddFile(SwiftEmptyFrameworkFileDestinatonRelativePath, SwiftEmptyFrameworkFileDestinatonRelativePath, PBXSourceTree.Source);

            proj.AddFileToBuild(targetGuidFramework, fileFrameworkGuid);
            if (targetGuidApplication != null)
            {
                proj.AddFileToBuild(targetGuidApplication, fileGuid);
            }
        }
        catch (IOException e)
        {
            Debug.LogError("Copying Swift file failed: " + e.Message);
        }
    }
}

#endif