using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

#if UNITY_2018_1_OR_NEWER
public class AATKitAndroidPreprocess : Editor, IPreprocessBuildWithReport
#else
public class AATKitPostprocess : Editor, IPreprocessBuild
#endif
{
    private static readonly string AndroidPluginsDirectoryPath = "/AATKit/SDK/Plugins/Android";

    public int callbackOrder 
    { 
        get 
        { 
            return 0; 
        }
    }

#if UNITY_2018_1_OR_NEWER

    public void OnPreprocessBuild(BuildReport report)
    {
        if (report.summary.platform == BuildTarget.Android)
        {
            OnPreprocessBuildForAndroidPlatform();
        }
    }

#else

    public void OnPreprocessBuild(BuildTarget target, string path)
    {
        if (target == BuildTarget.Android)
        {
            OnPreprocessBuildForAndroidPlatform();
        }
    }

#endif

    private static void OnPreprocessBuildForAndroidPlatform()
    {
        UnityEngine.Debug.Log("OnPreprocessBuild for Android platform.");

        string androidPath = Application.dataPath + AndroidPluginsDirectoryPath;
        DisableAndroidFiles(androidPath);
        EnableAndroidFiles(androidPath);

        AssetDatabase.Refresh();
    }

    private static void DisableAndroidFiles(string androidPath)
    {
        List<string> filesToDisable = AATKitUnitySettings.Instance.FilesToDisableForAndroid;
        foreach (string file in filesToDisable)
        {
            try
            {
                string actualPath = androidPath + "/" + file;
                string newPath = androidPath + "/" + file.Substring(0, file.Length - 4);
                if(!File.Exists(newPath))
                {
                    File.Move(actualPath, newPath);
                    File.Move(actualPath + ".meta", newPath + ".meta");
                }
            }
            catch (IOException e)
            {
                Debug.LogError("Failed to disable Android files: " + e.Message);
            }
        }
    }

    private static void EnableAndroidFiles(string androidPath)
    {
        List<string> filesToEnable = AATKitUnitySettings.Instance.FilesToEnableForAndroid;
        foreach (string file in filesToEnable)
        {
            try
            {
                string actualPath = androidPath + "/" + file.Substring(0, file.Length - 4);
                string newPath = androidPath + "/" + file;
                if(!File.Exists(newPath))
                {
                    File.Move(actualPath, newPath);
                    File.Move(actualPath + ".meta", newPath + ".meta");
                }
            }
            catch (IOException e)
            {
                Debug.LogError("Failed to enable Android files: " + e.Message);
            }
        }
    }
}