﻿using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    private static readonly float SecondsToSetRandomPosition = 1;

    private static readonly Vector2 MaxPositions = new Vector2(2,3);

    void Start()
    {
        StartCoroutine(ControllDirection());
    }

    private IEnumerator ControllDirection() 
    {
        while(true)
        {
            yield return new WaitForSeconds(SecondsToSetRandomPosition);

            float x = Random.Range(-1 * MaxPositions.x, MaxPositions.x);
            float y = Random.Range(-1 * MaxPositions.y, MaxPositions.y);

            transform.position = new Vector3(x, y, 0);
        }
    }
}
