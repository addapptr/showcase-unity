﻿using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AdsManager))]
public class GameManager : MonoBehaviour
{
    public static readonly int StartSceneIndex = 0;

    public static readonly int LaunchScreenSceneIndex = 1;

    public static readonly int MenuSceneIndex = 2;

    public static readonly int GameSceneIndex = 3;

    public static GameManager Instance
    {
        get;
        private set;
    }

    public static AdsManager AdsManager
    {
        get
        {
            return Instance.adsManager;
        }
    }

    private AdsManager adsManager;

    void Awake()
    {
        if(Instance == null) 
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
            adsManager = GetComponent<AdsManager>();
        }
        else 
        {
            Destroy(gameObject);
        }
    }
}
