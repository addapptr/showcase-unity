﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AdsManager : MonoBehaviour
{
    private static readonly string BannerPlacementAndroid = "Banner-android-unity";

    private static readonly string FullscreenPlacementAndroid = "Fullscreen-android-unity";

    private static readonly string BannerPlacementiOS = "Banner-ios-unity";

    private static readonly string FullscreenPlacementiOS = "Fullscreen-ios-unity";

    private static readonly string MenuSceneName = "Menu";

    private static string BannerPlacement
    {
        get
        {
            if(Application.platform == RuntimePlatform.Android)
            {
                return BannerPlacementAndroid;
            }
            else if(Application.platform == RuntimePlatform.IPhonePlayer)
            {
                return BannerPlacementiOS;
            }

            return "Banner";
        }
    }

    private static string FullscreenPlacement
    {
        get
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                return FullscreenPlacementAndroid;
            }
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                return FullscreenPlacementiOS;
            }

            return "Fullscreen";
        }
    }

    public bool ConsentRequired
    {
        get
        {
            return consentRequired;
        }
    }

    private bool consentRequired = false;

    private bool fullscreenAdIsDisplayed = false;

    void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void Start()
    {
        InitializeAds();
    }

    private void InitializeAds()
    {
        InitAATKit();
        CreatePlacements();
    }

    private void InitAATKit()
    {
        AATKitConfiguration configuration = new AATKitConfiguration()
        {
            Consent = new AATKitConsent()
            {
                Type = AATKitConsent.ConsentTypes.ManagedCMPGoogle
            }
        };
        AATKitBinding.Init(configuration);
        AATKitBinding.OnManagedConsentCMPFinishedDelegate += OnManagedConsentCompletion;
        AATKitBinding.OnManagedConsentCMPFailedToShowDelegate += OnManagedConsentFailed;
        AATKitBinding.OnManagedConsentCMPFailedToLoadDelegate += OnManagedConsentFailed;
        AATKitBinding.OnManagedConsentNeedsUserInterfaceDelegate += OnManagedConsentNeedsUserInterfaceDelegate;
    }

    private void CreatePlacements()
    {
        AATKitBinding.CreatePlacement(FullscreenPlacement, AATKitBinding.PlacementSize.Fullscreen);
        AATKitBinding.CreatePlacement(BannerPlacement, AATKitBinding.PlacementSize.BannerAuto);
    }

    public void StartAdsAutoReload()
    {
        StartFullscreenAdAutoReload();
        StartBannerAutoReload();
    }

    public void StartBannerAutoReload()
    {
        AATKitBinding.SetPlacementAlignment(BannerPlacement, AATKitBinding.BannerAlignment.BottomCenter);
        AATKitBinding.StartPlacementAutoReload(BannerPlacement);
    }

    public void StartFullscreenAdAutoReload()
    {
        AATKitBinding.StartPlacementAutoReload(FullscreenPlacement);
    }

    public bool IsFrequencyCapReachedForFullscreenAd()
    {
        return AATKitBinding.IsFrequencyCapReachedForPlacement(FullscreenPlacement);
    }

    public void ReloadFullscreenAd()
    {
        AATKitBinding.ReloadPlacement(FullscreenPlacement);
    }

    public void ShowFullscreenAd()
    {
        fullscreenAdIsDisplayed = AATKitBinding.ShowPlacement(FullscreenPlacement);
    }

    public void ConfigureConsent()
    {
        AATKitBinding.EditConsent();
    }

    private void OnManagedConsentNeedsUserInterfaceDelegate()
    {
        AATKitBinding.ShowConsentDialogIfNeeded();
        consentRequired = true;
    }

    private void OnManagedConsentCompletion(AATKitBinding.ManagedConsentState state)
    {
        LoadMenuSceneIfNeeded();
    }

    private void OnManagedConsentFailed(string error)
    {
        Debug.LogError("Managed consent failed to load, error: " + error);
        LoadMenuSceneIfNeeded();
    }

    private void LoadMenuSceneIfNeeded()
    {
        if (SceneManager.GetActiveScene().buildIndex == GameManager.StartSceneIndex)
        {
            StartAdsAutoReload();
            SceneManager.LoadScene(GameManager.MenuSceneIndex);
        }
    }

    public void HideBanner()
    {
        AATKitBinding.RemovePlacementFromView(BannerPlacement);
    }

    public void ShowBanner()
    {
        AATKitBinding.AddPlacementToView(BannerPlacement);
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus)
        {
            if(fullscreenAdIsDisplayed)
            {
                fullscreenAdIsDisplayed = false;
            }
            else
            {
                SceneManager.LoadScene(GameManager.StartSceneIndex);
            }
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if(scene.name == MenuSceneName) 
        {
            ShowBanner();
        }
    }
}