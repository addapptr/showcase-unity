﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour
{
    void Start()
    {
        if (!GameManager.AdsManager.IsFrequencyCapReachedForFullscreenAd())
        {
            GameManager.AdsManager.ReloadFullscreenAd();
            SceneManager.LoadScene(GameManager.LaunchScreenSceneIndex);
        }
        else
        {
            GameManager.AdsManager.StartAdsAutoReload();
            SceneManager.LoadScene(GameManager.MenuSceneIndex);
        }
    }
}
