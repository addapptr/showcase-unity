﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ConfigureConsentButton : MonoBehaviour
{
    public void OnClick() 
    {
        GameManager.AdsManager.ConfigureConsent();
    }
}
