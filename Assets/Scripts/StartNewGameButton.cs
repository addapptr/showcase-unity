﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartNewGameButton : MonoBehaviour
{
    public void OnClick() 
    {
        GameManager.AdsManager.ShowFullscreenAd();
        SceneManager.LoadSceneAsync(GameManager.GameSceneIndex);
    }
}
