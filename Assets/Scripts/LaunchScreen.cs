﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LaunchScreen : MonoBehaviour
{
    private static readonly float TimeInSecondsWhenLoadMenu = 7;

    public Text loadingPercentText;

    private float deltaTime;

    void Start()
    {
        StartCoroutine(LoadMenu());
        GameManager.AdsManager.StartFullscreenAdAutoReload();
        GameManager.AdsManager.HideBanner();
    }

    IEnumerator LoadMenu() 
    {
        yield return new WaitForSeconds(TimeInSecondsWhenLoadMenu);
        if(!GameManager.AdsManager.ConsentRequired)
        {
            GameManager.AdsManager.ShowFullscreenAd();
        }
        GameManager.AdsManager.StartAdsAutoReload();
        SceneManager.LoadSceneAsync(GameManager.MenuSceneIndex);
    }

    void Update()
    {
        deltaTime += Time.deltaTime;
        int progress = (int)(deltaTime * 100 / TimeInSecondsWhenLoadMenu);

        if(progress > 100)
        {
            progress = 100;
        }

        loadingPercentText.text = progress + "%";
    }
}